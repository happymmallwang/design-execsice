# 工厂方法模式
## 结构和角色
![img.png](img.png)

抽象工厂(creator)
具体工厂(Concrete Creator)
抽象产品(product)
具体产品(Concrete Product)

### 核心是抽象的工厂类 简单工厂的核心是具体的工厂类  简言之就是多个简单工厂的综合