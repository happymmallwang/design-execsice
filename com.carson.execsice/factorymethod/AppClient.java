package com.carson.execsice.factorymethod;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 11:22
 * app 客户类
 **/
public class AppClient {
    private static Concretor concretorO, concretorT;
    private static Product productO, productT;

    public static void main(String[] args) {
        concretorT = new ConcreteConcretorT();
        productT = concretorT.buildFactory();

        concretorO = new ConcreteConcretorO();
        productO = concretorO.buildFactory();

    }
}
