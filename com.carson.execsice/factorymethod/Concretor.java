package com.carson.execsice.factorymethod;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 11:01
 **/
public interface Concretor {
    //子类业务实现
    Product buildFactory();
}
