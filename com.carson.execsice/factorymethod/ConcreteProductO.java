package com.carson.execsice.factorymethod;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 11:18
 **/
public class ConcreteProductO implements Product{
    public ConcreteProductO(){
        System.out.println("我是产品productO 的业务");
    }
}
