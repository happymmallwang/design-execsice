package com.carson.execsice.factorymethod;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 11:18
 **/
public class ConcreteConcretorO implements Concretor{
    @Override
    public Product buildFactory() {
        return new ConcreteProductO();
    }
}
