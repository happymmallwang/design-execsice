package com.carson.execsice.factorymethod;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 11:18
 **/
public class ConcreteProductT implements Product{
    public ConcreteProductT(){
        System.out.println("我是产品productT 的业务");
    }
}
