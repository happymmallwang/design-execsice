package com.carson.execsice.singlemodel;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:31
 *  todo 但这种实现并不高效，因为任何时候调用都必须承受同步带来的性能开销，然而又只有第一次调用需要同步，所以并不建议使用
 **/
public class LazySingleton {
    private static LazySingleton lazySingleton = null;

    /**
     * 无参构造函数
     */
    private LazySingleton(){};

    synchronized public static LazySingleton getInstance(){
        if (lazySingleton==null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }
}
