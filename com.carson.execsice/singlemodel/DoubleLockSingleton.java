package com.carson.execsice.singlemodel;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 15:57
 * 双重检索版单例 建议使用
 **/
public class DoubleLockSingleton {
    private static volatile DoubleLockSingleton doubleLockSingleton = null;

    private DoubleLockSingleton(){};

    public static DoubleLockSingleton getInstance(){
        if (doubleLockSingleton==null){
            synchronized (com.carson.execsice.singlemodel.UpSetMultiton.class){
                if (doubleLockSingleton == null){
                    doubleLockSingleton = new DoubleLockSingleton();
                }
            }
        }
        return doubleLockSingleton;
    }
}
