package com.carson.execsice.singlemodel;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:23
 * 饿汉式
 **/
public class EagerSingleton {
    private static  final EagerSingleton eagerSingleTon = new EagerSingleton();

    /**
     * 无参构造
     */
    private EagerSingleton(){};

    //获取单例方法
    public EagerSingleton getInstance(){
        return eagerSingleTon;
    }
}
