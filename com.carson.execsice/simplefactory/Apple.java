package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 09:45
 * 苹果
 **/
public class Apple implements Fruit {
    //子類特有的方法 树龄
    private Integer treeAge;

    public Integer getTreeAge(){
        return this.treeAge;
    }

    public void  setTreeAge(Integer treeAge){
        this.treeAge = treeAge;
    }

    @Override
    public void grow() {
        System.out.println("苹果生长了!!!");
    }

    @Override
    public void water() {
        System.out.println("給苹果浇水!!!");
    }
}
