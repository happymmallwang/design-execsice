package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 09:45
 * 香蕉
 **/
public class Bnana implements Fruit {

    @Override
    public void grow() {
        System.out.println("香蕉生长了!!!");
    }

    @Override
    public void water() {
        System.out.println("給香蕉浇水!!!");
    }
}
