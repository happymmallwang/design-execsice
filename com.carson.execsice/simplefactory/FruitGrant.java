package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 10:07
 **/
public class FruitGrant {
    public static Fruit factory(String which) throws BadFruitException{
        if ("apple".equalsIgnoreCase(which)){
            return new Apple();
        }
        if ("bnana".equalsIgnoreCase(which)){
            return new Bnana();
        }
        if ("grape".equalsIgnoreCase(which)){
            return new Grape();
        }
        throw new BadFruitException("bad fruit");
    }
}
