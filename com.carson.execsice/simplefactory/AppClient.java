package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 10:13
 **/
public class AppClient {
    public static void main(String[] args) throws BadFruitException {
        Apple apple = (Apple)FruitGrant.factory("apple");
        Fruit grape = FruitGrant.factory("grape");
        Fruit bnana = FruitGrant.factory("bnana");
//        Fruit ex = FruitGrant.factory("ex");
        apple.grow();
        apple.setTreeAge(1);
        System.out.println(apple.getTreeAge());
        grape.grow();
        bnana.grow();
//        ex.grow();
    }
}
