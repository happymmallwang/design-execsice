package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 09:43
 **/
public interface Fruit {
    //生长
    void grow();
    //浇水
    void water();
}
