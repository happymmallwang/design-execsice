package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 09:45
 * 葡萄
 **/
public class Grape implements Fruit {

    @Override
    public void grow() {
        System.out.println("葡萄生长了!!!");
    }

    @Override
    public void water() {
        System.out.println("給葡萄浇水!!!");
    }
}
