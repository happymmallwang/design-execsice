package com.carson.execsice.simplefactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 10:08
 **/
public class BadFruitException extends Exception{
    public BadFruitException(String msg){
        super(msg);
    }
}
