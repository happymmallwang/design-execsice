package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:08
 **/
public class ProductAO implements ProductA{
    public ProductAO(){
        System.out.println("我是ProductAO的业务代码");
    }
}
