package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:13
 **/
public class ConcreteConcretorO implements Concrete{
    @Override
    public ProductA factoryA() {
        return new ProductAO();
    }

    @Override
    public ProductB factoryB() {
        return new ProductBO();
    }
}
