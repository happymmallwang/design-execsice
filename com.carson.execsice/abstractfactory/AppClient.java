package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:16
 * app 客户端
 **/
public class AppClient {
    public static void main(String[] args) {
        Concrete concreteConcretorO = new ConcreteConcretorO();
        Concrete concreteConcretorT = new ConcreteConcretorT();
        ProductA productA = concreteConcretorO.factoryA();
        ProductB productB = concreteConcretorO.factoryB();
        ProductA productA1 = concreteConcretorT.factoryA();
        ProductB productB1 = concreteConcretorT.factoryB();
    }
}
