package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:08
 **/
public class ProductBO implements ProductB{
    public ProductBO(){
        System.out.println("我是ProductBO的业务代码");
    }
}
