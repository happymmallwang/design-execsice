package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:08
 **/
public class ProductBT implements ProductB{
    public ProductBT(){
        System.out.println("我是ProductBT的业务代码");
    }
}
