package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:05
 * 工厂角色
 **/

public interface Concrete {
    //构造A工厂类
    ProductA factoryA();

    //构造B工厂
    ProductB factoryB();
}
