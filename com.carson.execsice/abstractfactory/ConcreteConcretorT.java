package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:13
 **/
public class ConcreteConcretorT implements Concrete{
    @Override
    public ProductA factoryA() {
        return new ProductAT();
    }

    @Override
    public ProductB factoryB() {
        return new ProductBT();
    }
}
