package com.carson.execsice.abstractfactory;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 13:08
 **/
public class ProductAT implements ProductA{
    public ProductAT(){
        System.out.println("我是ProductAT的业务代码");
    }
}
