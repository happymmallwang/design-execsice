package com.carson.execsice.multitonmodel;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/14 15:57
 * 双重检索版单例 建议使用
 **/
public class UpSetMultiton {
    private static Die dieOne = new Die();
    private static Die dieTwo = new Die();

    private UpSetMultiton(){};

    public static Die getInstance(String type){
        if ("one".equalsIgnoreCase(type)){
          return dieOne;
        }else {
            return dieTwo;
        }
    }
}
