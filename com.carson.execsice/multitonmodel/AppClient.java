package com.carson.execsice.multitonmodel;

import com.carson.execsice.singlemodel.UpSetMultiton;

/**
 * @author wy_chunyang_wang
 * @date 2022/09/23 10:53
 **/
public class AppClient {
    public static void main(String[] args) {
        Die one = UpSetMultiton.getInstance("one");
        Die two = UpSetMultiton.getInstance("two");
        System.out.println(one+one.dieDemo());
        System.out.println(two+two.dieDemo());

    }
}
